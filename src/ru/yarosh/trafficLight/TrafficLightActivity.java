package ru.yarosh.trafficLight;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Button;

public class TrafficLightActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void onClickButtonBackgroundRed(View view){
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.generalLayout);
        mainLayout.setBackgroundColor(Color.RED);
    }

    public void onClickButtonBackgroundYellow(View view){
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.generalLayout);
        mainLayout.setBackgroundColor(Color.YELLOW);
    }

    public void onClickButtonBackgroundGreen(View view){
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.generalLayout);
        mainLayout.setBackgroundColor(Color.GREEN);
    }
}
